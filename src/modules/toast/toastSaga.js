import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'
import helpers from '../utils/helpers';


function* showToast(action) {
    yield put({ type: 'SHOW_TOAST', payload: action.payload });
}


function* hideToast() {
    yield put({ type: 'HIDE_TOAST' });
}


function* toastSaga() {
    yield takeEvery('HIDE_TOAST', showToast);
    yield takeEvery('SHOW_TOAST', hideToast);
}

export default toastSaga;
