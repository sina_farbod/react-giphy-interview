const initialState = {
	toast: '',
}

export default function toastReducer(state = initialState, action) {
	switch (action.type) {

		case 'SHOW_TOAST':
			return {
				...state,
				toast: action.payload,
			}

		case 'HIDE_TOAST':
			return {
				...state,
				toast: '',
			}

		 
		default:
			return state;
	}
}