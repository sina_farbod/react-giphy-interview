
export function showToast(payload) {
    return {
        type: 'SHOW_TOAST',
        payload
    }
}

export function hideToast() {
    return {
        type: 'HIDE_TOAST',
    }
}