import React from 'react';
import { Toast } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function MyToast(props) {
	return (
		<div
			aria-live="polite"
			aria-atomic="true"
			style={{
				position: 'fixed',
				zIndex: 9999,
				minHeight: '100px',
				width: '100%',
				marginTop: 60
			}}
		>
			<Toast onClose={() => props.close()} show={props.showModal} delay={90000} autohide className="mx-auto mt-1 ">
				<Toast.Header className={`text-white ${props.headerBg}`}>
					<strong className="mx-auto rtl">
						{
							props.msg && props.msg !== 401 && "Error!"
						}
					</strong>
				</Toast.Header>
				<Toast.Body className="rtl text-justify">
					{
						props.msg === 401 ?
							<Link to="/login" className="text-primary mr-2 d-md-none">
								Login
							</Link>
							:
							props.msg
					}
				</Toast.Body>
			</Toast>
		</div>
	);
}