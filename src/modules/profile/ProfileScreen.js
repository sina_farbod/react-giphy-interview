import React, { useEffect } from 'react';
import { Alert, Col, Container, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Header from '../../components/Header';
import Loaders from '../../components/Loaders';

const ProfileScreen = () => {

	const dispatch = useDispatch();
	const { loadingProfile, errorProfile, user } = useSelector(state => state.profileReducer)

	useEffect(() => {

		dispatch({ type: "GET_PROFILE_REQUEST" })

	}, [])



	return (
		<div className="wrapper ProfileScreen auth" style={{ height: "100vh" }}>
			<Header title="ورود" left="menu" />

			<div className="main-container mt-5">
				<Container fluid>
					<Row>
						<Col md={6} xs={12} className="main-content px-md-4 mx-auto">

							{
								errorProfile &&
								<Alert variant="danger">
									{errorProfile}
								</Alert>
							}
							<Row className="border-bottom mb-4 d-flex justify-content-between pb-3">

								{
									loadingProfile ? <Loaders.Spinner className="w-100 text-center mt-5" /> :
										<div className="text-center w-100">
											<img src={user.avatar} className="rounded-circle mb-3" />
											<h4>{user.first_name + ' ' + user.last_name}</h4>
										</div>
								}
							</Row>
							<Row className="">
								<p>Email:</p>
								<p>{user.email}</p>
							</Row>

						</Col>
					</Row>

				</Container>
			</div>
		</div>
	)


}
export default ProfileScreen