import { call, put, takeLatest } from 'redux-saga/effects';
import { request } from '../../utils/request';

function* getProfile() {
    try {

        let obj = {
            method: 'GET',
            url: `https://reqres.in/api/users/2`,
            headers: {
                'Authorization': localStorage.getItem('token'),
            },
        }
        const result = yield call(request, obj);

        if (result.status >= 400) {
            yield put({
                type: 'GET_PROFILE_FAILED',
                errors: result.data.errors
            })
            return
        }

        yield put({
            type: 'GET_PROFILE_SUCCESS',
            payload: result.data.data
        })

    } catch (e) {
        console.log('e', e)
        yield put({
            type: 'GET_PROFILE_FAILED',
            message: e.message
        });
    }
}
export default function* getProfileSaga() {
    yield takeLatest('GET_PROFILE_REQUEST', getProfile);
}


