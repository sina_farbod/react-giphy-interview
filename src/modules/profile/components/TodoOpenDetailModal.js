import { faCheckSquare, faMinus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';


export default function TodoOpenDetailModal(props) {
	return (
		<Modal show={props.showModal} onHide={() => props.close()} centered>
			<Modal.Header closeButton className={props.bgHeader || ''}>
				<Modal.Title className="h6">Todo Detail</Modal.Title>
			</Modal.Header>
			<Modal.Body className="rtl">

				<div className="d-flex my-3">
					<FontAwesomeIcon
						size="lg"
						icon={props.item.completed ? faCheckSquare : faMinus}
						className={`${props.item.completed ? "text-success" : "text-secondary"} mr-4`}
					/>

					<h6 className={`${props.item.completed ? "text-success" : "text-secondary"} mb-0 text-left`}>
						{props.item.title}
					</h6>
				</div>

				<div className="d-flex">
					<p className="text-bold">Status:</p>
					<span className="ml-2">{props.item.completed ? "Completed" : "Pending"}</span>
				</div>
			</Modal.Body>
			<Modal.Footer className="rtl">
				<Button variant="outline-primary" onClick={() => props.close()}>ok</Button>
			</Modal.Footer>
		</Modal>
	)
}