import React, { useEffect, useState } from 'react';
import { Alert, Button, Form, Modal, Spinner } from 'react-bootstrap';
import { useForm } from "react-hook-form";
import { useDispatch } from 'react-redux';
import { generateRandomNumber } from '../../../utils/helpers';
import { showToast } from '../../toast/toastAction';

export default function TodoAddModal(props) {
	const [loading, setLoading] = useState(false);
	const [amount, setAmount] = useState("10000");

	const [error, setError] = useState(null);


	const { register, formState: { errors }, handleSubmit } = useForm();
	const dispatch = useDispatch()

	const onChangeAmount = async (e) => {
		let val = e.target.value
		setAmount(val)
	}


	const _onSubmit = async (data) => {
		try {
			let todoList = localStorage.getItem('todoList')

			if (!todoList) {
				todoList = []
			}
			else {
				todoList = JSON.parse(todoList)
			}
			todoList.push({
				id: generateRandomNumber(),
				title: data.title,
				completed: false
			})

			localStorage.setItem('todoList', JSON.stringify(todoList))
			dispatch({ type: "GET_TODOS_REQUEST" })
			props.close()

		} catch (e) {
			console.log('e', e)
			dispatch(showToast("asdsads"))
		}
	}


	return (
		<Modal show={props.showModal} onHide={() => props.close()} centered>
			<Form noValidate onSubmit={handleSubmit(_onSubmit)}>
				<Modal.Header closeButton className={props.bgHeader || ''}>
					<Modal.Title className="h6">Todo add</Modal.Title>
				</Modal.Header>
				<Modal.Body className="rtl">
					{
						error &&
						<Alert variant="danger">
							{error}
						</Alert>
					}

					<Form.Group>
						<Form.Label >To-do Title</Form.Label>
						<Form.Control
							className="f13"
							placeholder="title"
							{...register("title", { required: true })}
						/>
					</Form.Group>
					{errors.title && <p className="text-danger">Title is required</p>}
				</Modal.Body>
				<Modal.Footer className="rtl">
					<Button variant="outline-primary" onClick={() => props.close()}>Cancel</Button>
					<Button type="submit" variant="primary" disabled={!!loading}>
						{
							loading ? <Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true" /> : "Submit"
						}
					</Button>
				</Modal.Footer>
			</Form>
		</Modal>
	)
}