const initialState = {
	loadingProfile: false,
	user: {},
	errorProfile: null,
}

export default function profileReducer(state = initialState, action) {
	switch (action.type) {

		case 'GET_PROFILE_REQUEST':
			return {
				...state,
				loadingProfile: true,
			}
		case 'GET_PROFILE_SUCCESS':
			return {
				...state,
				loadingProfile: false,
				user: action.payload,
				errorProfile: null,
			}
		case 'GET_PROFILE_FAILED':
			return {
				...state,
				loadingProfile: false,
				errorProfile: action.errors,
			}


		default:
			return state
	}
}