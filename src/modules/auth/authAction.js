
export function getUsersAction() {
    return {
        type: 'GET_USERS_REQUESTED',
    }
}

export function loginAction(payload) {
    return {
        type: 'USER_LOGIN_REQUEST',
        payload
    }
}

export function registerAction(payload) {
    return {
        type: 'USER_REGISTER_REQUEST',
        payload
    }
}
 
 

export function isLoginAction(payload) {
    return {
        type: 'IS_LOGIN',
        payload
    }
}
export function notLoginAction() {
    return {
        type: 'NOT_LOGIN',
    }
}

 