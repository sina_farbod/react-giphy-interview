import { push } from 'connected-react-router';
import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { request } from "../../utils/request";



function* loginWorker(action) {
    try {
        let obj = {
            method: 'POST',
            url: `https://reqres.in/api/login`,
            data: action.payload
        }
        const result = yield call(request, obj);

        if (result.status >= 400) {
            yield put({
                type: 'USER_LOGIN_FAILED',
                errors: result.data.error
            });
            yield put({
                type: 'SHOW_TOAST',
                payload: result.data.error
            });
            return
        }


        yield localStorage.setItem("token", "Bearer " + result.data.token)

        yield call(isLogin);
        yield put({
            type: 'USER_LOGIN_SUCCESS',
            user: result.data.data
        })
        yield put(push("/profile"))

    } catch (e) {
        console.log('e', e)
        yield put({ type: 'USER_LOGIN_FAILED', message: e.message });
    }
}
export function* loginSaga() {
    yield takeLatest('USER_LOGIN_REQUEST', loginWorker)
}


function* registerWorker(action) {
    try {
        let obj = {
            method: 'POST',
            url: `https://reqres.in/api/register`,
            data: action.payload
        }
        const result = yield call(request, obj);

        console.log(`result`, result)

        if (result.status >= 400) {
            yield put({
                type: 'USER_REGISTER_FAILED',
                errors: result.data.error
            });
            yield put({
                type: 'SHOW_TOAST',
                payload: result.data.error
            });

            return
        }


        yield localStorage.setItem("token", "Bearer " + result.data.token)

        yield call(isLogin);
        yield put({
            type: 'USER_REGISTER_SUCCESS',
            user: result.data.data
        })

        yield put(push("/profile"))

    } catch (e) {
        console.log('e', e)
        yield put({
            type: 'USER_REGISTER_FAILED',
            message: e.message
        });
    }
}
export function* registerSaga() {
    yield takeLatest('USER_REGISTER_REQUEST', registerWorker)
}

function* isLogin() {
    if (localStorage.getItem('token')) {
        console.log("isloginnnn")
        yield put({ type: 'IS_LOGIN_SUCCESS' });
    }
    else {
        notLogin()
    }
}
function* notLogin() {
    console.log("not login")
    yield put({ type: 'IS_LOGIN_FAILED' });
}


