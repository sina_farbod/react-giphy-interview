const initialState = {
	loading: false,
	user: {},
	error: null,
}

export default function loginReducer(state = initialState, action) {
	console.log(`action`, action)
	switch (action.type) {

		case 'USER_LOGIN_REQUEST':
			return {
				...state,
				loading: true,
			}
		case 'USER_LOGIN_SUCCESS':
			return {
				...state,
				loading: false,
				user: action.user,
				error: null,
			}
		case 'USER_LOGIN_FAILED':
			return {
				...state,
				loading: false,
				error: action.errors,
			}


		default:
			return state
	}
}