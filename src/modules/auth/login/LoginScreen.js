import React, { Component } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Header from '../../../components/Header';
import LoginForm from '../components/LoginForm';

class LoginScreen extends Component {
	render() {

		return (
			<div className="wrapper LoginScreen auth" style={{ height: "100vh" }}>
				<Header title="ورود" left="menu" />

				<div className="main-container">
					<Container fluid>

						<Row>
							<Col md={6} xs={12} className="main-content px-md-4 mx-auto">
								<div className="content-wrapper">
									<LoginForm />
								</div>
							</Col>
						</Row>

					</Container>
				</div>
			</div>
		)
	}
}
export default LoginScreen