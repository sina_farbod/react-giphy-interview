const initialState = {
	loadingRegister: false,
	user: {},
	errorRegister: null,
}

export default function registerReducer(state = initialState, action) {
	console.log(`action`, action)
	switch (action.type) {

		case 'USER_REGISTER_REQUEST':
			return {
				...state,
				loadingRegister: true,
			}
		case 'USER_REGISTER_SUCCESS':
			return {
				...state,
				loadingRegister: false,
				user: action.user,
				errorRegister: null,
			}
		case 'USER_REGISTER_FAILED':
			return {
				...state,
				loadingRegister: false,
				errorRegister: action.errors,
			}


		default:
			return state
	}
}