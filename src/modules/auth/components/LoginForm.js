import React, { useEffect, useState } from 'react';
import { Alert, Button, Card, Form } from 'react-bootstrap';
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from "react-router-dom";
import Loaders from '../../../components/Loaders';
import { hideToast } from '../../toast/toastAction';
import { loginAction } from '../authAction';
// import { request } from '../utils/request-util';


const LoginForm = (props) => {
	const [dataProvider, setDataProvider] = useState();
	const history = useHistory()
	const dispatch = useDispatch()
	const [mobile, setMobile] = useState('');
	const [password, setPassword] = useState('');
	const { register, formState: { errors }, handleSubmit } = useForm();

	const { loading, error, user } = useSelector(state => state.loginReducer)


	const _onSubmit = async (data) => {
		let obj = {
			email: data.email,
			password: data.password
		}
		dispatch(loginAction(obj))
	}


	return (
		<div xs={12} md={5} className="mb-2 mt-5">
			{
				error &&
				<Alert variant="danger">
					{error}
				</Alert>
			}

			<Card>
				<Card.Body>
					<p className="text-center mt-2 mb-4">Please enter your username and password</p>

					<Form noValidate onSubmit={handleSubmit(_onSubmit)}>
						<Form.Group>
							<Form.Control
								type="email"
								placeholder="email"
								className="f13"
								{...register("email", { required: true })}
							/>
							{errors.email && errors.email.type === "required" && <p className="text-danger">email is required.</p>}
						</Form.Group>


						<Form.Group>
							<Form.Control
								name="password"
								type="password"
								className="f13"
								placeholder="password"
								{...register("password", { required: true })}
							/>
							{errors.password && <p className="text-danger m-0">password field is required.</p>}
						</Form.Group>

						<Button type="submit" block className="mb-2" size="lg" disabled={!!loading}>
							{
								!loading ? "Submit" : <Loaders.Spinner />
							}
						</Button>

						<Link to="/register" className="text-primary mb-4">
							Not registered? register here
						</Link>


					</Form>

				</Card.Body>
			</Card>

		</div>


	);
}
export default LoginForm;