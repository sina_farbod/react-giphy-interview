import React, { useEffect, useState } from 'react';
import { Alert, Button, Card, Form } from 'react-bootstrap';
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from "react-router-dom";
import Loaders from '../../../components/Loaders';
import { registerAction } from '../authAction';
// import { request } from '../utils/request-util';


const RegisterForm = (props) => {
	const [dataProvider, setDataProvider] = useState();
	const history = useHistory()
	const dispatch = useDispatch()
	const [mobile, setMobile] = useState('');
	const [password, setPassword] = useState('');
	const { register, formState: { errors }, handleSubmit } = useForm();

	const { loadingRegister, errorRegister, user } = useSelector(state => state.registerReducer)


	const _onSubmit = async (data) => {
		let obj = {
			email: data.email,
			password: data.password
		}
		dispatch(registerAction(obj))
	}


	return (
		<div xs={12} md={5} className="mb-2 mt-5">
			{
				errorRegister &&
				<Alert variant="danger">
					{errorRegister}
				</Alert>
			}

			<Card>
				<Card.Body>
					<p className="text-center mt-2 mb-4">Please enter your username and password</p>

					<Form noValidate onSubmit={handleSubmit(_onSubmit)}>
						<Form.Group>
							<Form.Control
								type="email"
								placeholder="email"
								className="f13"
								{...register("email", { required: true })}
							/>
							{errors.email && errors.email.type === "required" && <p className="text-danger">email is required.</p>}
						</Form.Group>


						<Form.Group>
							<Form.Control
								name="password"
								type="password"
								className="f13"
								placeholder="password"
								{...register("password", { required: true })}
							/>
							{errors.password && <p className="text-danger m-0">password field is required.</p>}
						</Form.Group>

						<Button type="submit" block className="mb-2" size="lg" disabled={!!loadingRegister}>
							{
								!loadingRegister ? "Submit" : <Loaders.Spinner />
							}
						</Button>

						<Link to="/login" className="text-primary mb-4">
							Already registered? login here
						</Link>


					</Form>

				</Card.Body>
			</Card>

		</div>


	);
}
export default RegisterForm;