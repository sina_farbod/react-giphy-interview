const initialState = {
	loadingTodos: false,
	todoList: [],
	errorTodos: null,
}

export default function todosReducer(state = initialState, action) {
	switch (action.type) {

		case 'GET_TODOS_REQUEST':
			return {
				...state,
				loadingTodos: true,
			}
		case 'GET_TODOS_SUCCESS':
			return {
				...state,
				loadingTodos: false,
				todoList: action.payload,
				errorTodos: null,
			}
		case 'GET_TODOS_FAILED':
			return {
				...state,
				loadingTodos: false,
				errorTodos: action.errors,
			}


		default:
			return state
	}
}