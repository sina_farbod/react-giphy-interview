import { push } from 'connected-react-router';
import { call, debounce, put, takeLatest } from 'redux-saga/effects';
import { getParamsFromUrl } from '../../utils/helpers';
import { request } from '../../utils/request';
import config from "../../config.json";
// import { bourseKalaResetAction } from '../actions/bourseKalaAction';

function* getTodoList({ payload }) {
    try {
        const todoList = localStorage.getItem('todoList')

        yield put({
            type: 'GET_TODOS_SUCCESS',
            payload: JSON.parse(todoList)
        })

    } catch (e) {
        console.log('e', e)
        yield put({ type: 'GET_TODOS_FAILED', message: e.message });
    }
}
export default function* getTodoListSaga() {
    yield takeLatest('GET_TODOS_REQUEST', getTodoList);
}


 