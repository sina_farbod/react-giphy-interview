import React, { useEffect } from 'react';
import { Alert, Col, Container, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Header from '../../components/Header';
import Loaders from '../../components/Loaders';
import TodoAddButton from './components/TodoAddButton';
import TodoListItem from './components/TodoListItem';

const TodoListScreen = () => {

	const dispatch = useDispatch();
	const { loadingTodos, errorTodos, todoList } = useSelector(state => state.todosReducer)

	console.log(`loadingTodos, todoList`, loadingTodos, todoList)
	useEffect(() => {

		dispatch({ type: "GET_TODOS_REQUEST" })

	}, [])



	return (
		<div className="wrapper TodoListScreen auth" style={{ height: "100vh" }}>
			<Header title="ورود" left="menu" />

			<div className="main-container mt-5">
				<Container fluid>
					<Row>
						<Col md={6} xs={12} className="main-content px-md-4 mx-auto">
							{
								errorTodos &&
								<Alert variant="danger">
									{errorTodos}
								</Alert>
							}
							<Row className="border-bottom mb-4 d-flex justify-content-between pb-3">
								<h3 className="text-center">
									To do List
								</h3>

								<TodoAddButton />
							</Row>
							{
								!loadingTodos && !todoList &&
								<p className="my-5 text-center">No items found in to dos.</p>
							}
							{
								!loadingTodos && todoList && todoList.length < 1 &&
								<p className="my-5 text-center">No items found in to dos.</p>
							}
							{
								loadingTodos ? <Loaders.Spinner className="w-100 text-center mt-5" /> :
									todoList && todoList.length > 0 && todoList.map((item, i) =>
										<TodoListItem item={item} key={i} />
									)
							}
						</Col>
					</Row>

				</Container>
			</div>
		</div>
	)


}
export default TodoListScreen