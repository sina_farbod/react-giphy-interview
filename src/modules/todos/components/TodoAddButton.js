import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import TodoAddModal from './TodoAddModal';



const TodoAddButton = ({ item }) => {
	const [addTodoModal, setAddTodoModal] = useState(false);

	return (
		<>
			<Button variant="info" onClick={() => setAddTodoModal(true)}>
				Add
			</Button>

			{
				addTodoModal &&
				<TodoAddModal
					showModal={addTodoModal}
					close={() => setAddTodoModal(false)}
				/>
			}
		</>
	)
}
export default TodoAddButton