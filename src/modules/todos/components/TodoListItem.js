import { faCheckSquare, faMinus, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from 'react';
import { Button, Card } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import styles from '../todos.module.scss';
import TodoOpenDetailModal from './TodoOpenDetailModal';



const TodoListItem = ({ item }) => {
	const [deleted, setDeleted] = useState(null);
	const [detailModal, setDetailModal] = useState(false);

	const onDelete = async (id) => {
		let arr = []
		let todoList = localStorage.getItem('todoList')
		arr = JSON.parse(todoList)
		var newarr = arr.filter(arr => (arr.id != id))
		localStorage.setItem('todoList', JSON.stringify(newarr))
		setDeleted(true)
	}

	return (
		<Card className={`search-result-item mb-3 ${deleted ? "d-none" : ""}`}>
			<Card.Body className="d-flex px-3">
				<Button
					block
					variant="link"
					className="text-black d-flex align-items-center rounded"
					onClick={() => setDetailModal(true)}
				>
					<FontAwesomeIcon
						size="lg"
						icon={item.completed ? faCheckSquare : faMinus}
						className={`${item.completed ? "text-success" : "text-secondary"} mr-4`}
					/>

					<h6 className={`${item.completed ? "text-success" : "text-secondary"} mb-0 text-left`}>
						{item.title}
					</h6>
				</Button>

				<span
					role="button"
					className={`${styles.deleteBtn}`}
					onClick={() => onDelete(item.id)}
				>
					<FontAwesomeIcon
						size="lg"
						color="red"
						icon={faTimesCircle}
						className=""
					/>
				</span>
			</Card.Body>

			{
				detailModal &&
				<TodoOpenDetailModal
					bgHeader="bg-info text-white"
					item={item}
					showModal={detailModal}
					close={() => setDetailModal(false)}
				/>
			}
		</Card>
	)
}
export default TodoListItem