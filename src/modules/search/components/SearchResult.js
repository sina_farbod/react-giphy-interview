import React from 'react';
import { Col, Form, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from "react-router-dom";
import Loaders from '../../../components/Loaders';
import SearchResultItem from './SearchResultItem';



const SearchResult = () => {

	const history = useHistory();
	const dispatch = useDispatch();

	const { loadingSearch, errorSearch, images } = useSelector(state => state.searchReducer)

	console.log(`loadingSearch, images`, loadingSearch, images)

	const onChangeSearch = (e) => {

	};


	if (loadingSearch) {
		return (
			<Loaders.Spinner className="w-100 text-center mt-5" />
		)
	}


	return (
		<Row className="search-result-container">
			{
				images.length > 0 && images.map((item, i) =>
					<Col xs={12} sm={6} md={4} className="mb-4" key={i}>
						<SearchResultItem item={item} />
					</Col>
				)
			}
		</Row>

	)
}
export default SearchResult