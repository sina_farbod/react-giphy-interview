import React from 'react';
import { Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from "react-router-dom";



const SearchInput = () => {

	const history = useHistory();
	const dispatch = useDispatch();

	// const { filter } = useSelector(state => state.userProductsActiveTabReducer)
	const { id } = useParams()

	const onChangeSearch = (e) => {
		let q = e.target.value;
		q = q.trim()

		dispatch({
			type: "CHANGE_SEARCH_Q_REQUEST",
			payload: {
				q: q ? q : '',
			}
		})
	};


	return (
		<div className="userdetails-search-input">
			<Form.Group>
				<Form.Control
					name="q"
					size="lg"
					placeholder="Search"
					onChange={onChangeSearch}
					className="f13 br10"
				/>
			</Form.Group>
		</div>

	)
}
export default SearchInput