import React from 'react';
import { Card, Form, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from "react-router-dom";
import Loaders from '../../../components/Loaders';



const SearchResultItem = ({ item }) => {

	const history = useHistory();
	const dispatch = useDispatch();

	console.log(`item`, item)

	const onChangeSearch = (e) => {

	};


	return (
		<Card className="search-result-item h-100">
			<Card.Body className="pt-1">
				<Row>
					<img src={item.images.downsized_medium.url} className="img-fluid rounded mb-2" />
				</Row>
				<h6 className="mb-0">{item.title}</h6>
			</Card.Body>
		</Card>
	)
}
export default SearchResultItem