import { push } from 'connected-react-router';
import { call, debounce, put, takeLatest } from 'redux-saga/effects';
import { getParamsFromUrl } from '../../utils/helpers';
import { request } from '../../utils/request';
import config from "../../config.json";
// import { bourseKalaResetAction } from '../actions/bourseKalaAction';

function* getGiphies({ payload }) {
    try {
        let q = payload.q ? payload.q : ''

        console.log(`payload`, payload)


        let obj = {
            method: 'GET',
            // baseURL: process.env.REACT_APP_LOCAL_URL,
            url: `https://api.giphy.com/v1/gifs/search?api_key=${config.api_key}&q=${q}&limit=${config.per}&offset=${payload.page ? payload.page : 1}&rating=g&lang=en`,
        }
        const result = yield call(request, obj);

        if (result.status >= 400) {
            yield put({
                type: 'SEARCH_FAILED',
                errors: result.data.errors
            })
            return
        }

        yield put({
            type: 'SEARCH_SUCCESS',
            payload: result.data.data
        })

    } catch (e) {
        console.log('e', e)
        yield put({ type: 'SEARCH_FAILED', message: e.message });
    }
}
export default function* getGiphiesSaga() {
    yield takeLatest('GET_GIPHIES_REQUEST', getGiphies);
}


function* changeSearchQWorker({ payload }) {

    yield put({
        type: 'CHANGE_SEARCH_Q_SUCCESS',
        payload: payload.q
    })

    yield put(push(`/? q = ${payload.q}`))

    yield put({
        type: 'GET_GIPHIES_REQUEST',
        payload
    })

    // yield put(bourseKalaRequestAction(payload))

}

export function* changeSearchQSaga() {
    yield debounce(350, 'CHANGE_SEARCH_Q_REQUEST', changeSearchQWorker);
}