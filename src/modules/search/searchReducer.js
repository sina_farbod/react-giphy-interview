const initialState = {
	loadingSearch: false,
	images: [],
	errorSearch: null,
}

export default function searchReducer(state = initialState, action) {
	switch (action.type) {

		case 'SEARCH_REQUEST':
			return {
				...state,
				loadingSearch: true,
			}
		case 'SEARCH_SUCCESS':
			return {
				...state,
				loadingSearch: false,
				images: action.payload,
				errorSearch: null,
			}
		case 'SEARCH_FAILED':
			return {
				...state,
				loadingSearch: false,
				errorSearch: action.errors,
			}


		default:
			return state
	}
}