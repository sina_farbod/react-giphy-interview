import React, { Component } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Header from '../../components/Header';
import SearchInput from './components/SearchInput';
import SearchResult from './components/SearchResult';

class SearchScreen extends Component {
	render() {

		return (
			<div className="wrapper SearchScreen auth" style={{ height: "100vh" }}>
				<Header title="ورود" left="menu" />

				<div className="main-container mt-5">
					<Container fluid>

						<Row>
							<Col md={6} xs={12} className="main-content px-md-4 mx-auto">
								<div className="content-wrapper">
									<SearchInput />
								</div>
							</Col>
						</Row>
						<Row>
							<Col md={6} xs={12} className="main-content px-md-4 mx-auto">
								<div className="serch-result">
									<SearchResult />
								</div>
							</Col>
						</Row>

					</Container>
				</div>
			</div>
		)
	}
}
export default SearchScreen