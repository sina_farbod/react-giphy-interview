import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import helpers from '../utils/helpers';
import { Link } from "react-router-dom";
import { Card, Button, Col, Spinner } from 'react-bootstrap';

let width = window.innerWidth;

const Loaders = {

	Spinner({ className }) {
		return (
			<div className={className ? className + " loader" : 'loader'}>
				<Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true" />
			</div>
		)
	},



}

export default Loaders
