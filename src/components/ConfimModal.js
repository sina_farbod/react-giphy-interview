import React, { useEffect } from 'react';
import { Button, Modal } from 'react-bootstrap';

export default function ConfimModal(props) {

	useEffect(() => {

	}, [])


	return (
		<Modal show={props.showModal} onHide={() => props.close()} centered>
			<Modal.Header closeButton className={props.bgHeader || ''}>
				<Modal.Title className="h6" style={{ marginRight: -20 }}>هشدار</Modal.Title>
			</Modal.Header>
			<Modal.Body className="rtl">
				<div className="text-right mb15">
					<p className="text-justify mt-3">
						{
							props.body ? props.body : "آیا از انجام این کار مطمئن هستید؟"
						}
					</p>
				</div>
			</Modal.Body>
			<Modal.Footer className="rtl">
				<Button variant="" onClick={() => props.close(false)}>خیر</Button>
				<Button variant="primary" onClick={() => props.close(true)}>بله</Button>
			</Modal.Footer>
		</Modal>
	)
}