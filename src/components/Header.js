import {  faUser } from '@fortawesome/free-regular-svg-icons';
import { faHome, faSearch, faShoppingBag, faSignInAlt, faTasks } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from 'react';
import { Container, Nav, Navbar, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const links = [
	{
		"title": "Search",
		"link": "/",
		"icon": faSearch,
		"needAuth": false
	},
	{
		"title": "Login",
		"link": "/login",
		"icon": faSignInAlt,
		"needAuth": false
	},
	{
		"title": "To Dos",
		"link": "/todo",
		"icon": faTasks,
		"needAuth": false
	},
	{
		"title": "Profile",
		"link": "/profile",
		"icon": faUser,
		"needAuth": false
	},

]
class Header extends Component {

	constructor(props) {
		super(props)
		this.state = {
			menuOpen: false,
		}
		// console.log('this.props.history', this.props.history)
	}


	render() {
		return (
			<header>

				<Navbar bg="light" expand="lg">
					<Container>
						<Navbar.Toggle aria-controls="basic-navbar-nav" />
						<Navbar.Collapse className="justify-content-between">
							<Nav className="">
								{
									links.map((item, i) =>
										<Link to={item.link} className="nav-link" key={i}>
											<FontAwesomeIcon icon={item.icon} className="mr-2" />
											{item.title}
										</Link>
									)
								}
							</Nav>
							<Navbar.Brand>Giphy Interview</Navbar.Brand>
						</Navbar.Collapse>
					</Container>
				</Navbar>

			</header>
		)


	}
}
export default Header