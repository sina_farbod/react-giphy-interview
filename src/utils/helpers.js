export const getParamsFromUrl = (want) => {
	const params = new URLSearchParams(window.location.search);
	return params.get(want) ? params.get(want) : ''
}

export const isEmptyObj = (obj) => {
	return Object.keys(obj).length === 0;
}

export const generateRandomNumber = () => {
	return Math.floor(Math.random() * (999 - 100 + 1) + 100);
}

export const showError = (errors) => {
	let str = '';
	if (typeof errors === 'object') {
		if (errors.hasOwnProperty('message')) {
			if (errors.message === 'Unauthenticated.') {
				str = "لطفا دوباره لاگین کنید.";
			}
		}
		else {
			const errorValues = Object.values(errors)
			errorValues.map((item) => {
				if (typeof item !== 'object') {
					str = str + item + "\n";
				}
				else {
					const err = Object.values(item)
					err.map((elem) => {
						str = str + elem + "\n";
					})
				}
			})
		}
	}
	else {
		str = errors
	}
	return str;
}