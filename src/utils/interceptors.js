import { configureStore } from '../store/configureStore';
import { EModal, showModalAction } from '../../store/actions/modalAction';

const globalErrorHandler = (response) => {
    if (!!response?.data?.code) {
        if (response.data.code === 401) {
            configureStore.dispatch(showModalAction({
                message: 'مشکلی در سیستم به وجود آمده است (Token Expired)',
                type: EModal.ERROR
            }))
            throw {
                code: response.data.code,
                message: response.msg
            };
        }
        else if (response.data.code === 200) {
            return response;
        }
        else {
            console.log(`GLOBALERRORGANDLER`, response)

            let msg = ''
            if (response.data.msg) {
                msg = response.data.msg
            }

            if (!response.config.url.includes("/bimehsalamat/info") && !response.config.url.includes("/bimehsalamat/membersfetch")) {
                configureStore.dispatch(showModalAction({
                    message: msg,
                    type: EModal.ERROR
                }))
            }
            throw {
                data: response.data,
                code: response.data.code,
                message: msg == "Request Call Failed" ? "خطای سرور" : msg
            };
        }
    } else {
        return response;
    }

}
export default globalErrorHandler
