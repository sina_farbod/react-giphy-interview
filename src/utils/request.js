import axios, { AxiosRequestConfig } from 'axios';
import config from '../config.json';



export const request = async (payload) => {
    let result = {}

    let obj = {}
    obj.method = payload.method
    obj.baseURL = payload.baseURL ? payload.baseURL : process.env.REACT_APP_HAMI_PROXY_URL
    obj.url = payload.url
    obj.data = payload.data
    obj.headers = payload.headers



    console.log(`${payload.method} ${obj.baseURL ? obj.baseURL : ''} ${obj.url}`)

    try {
        const response = await axios(obj);
        console.log(`${obj.url} RESPONSE =>>>>`, response);

        result.status = response.status
        result.data = response.data

        return result

    } catch (error) {
        console.log('error =>>>>', error.response.status + error)


        result.status = error.response.status
        result.data = error.response.data
        return result
        // throw error;
    }
};
