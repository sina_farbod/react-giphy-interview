import { all, fork } from 'redux-saga/effects';
import { loginSaga, registerSaga } from '../modules/auth/authSaga';
import getProfileSaga from '../modules/profile/profileSaga';
import getGiphiesSaga, { changeSearchQSaga } from '../modules/search/SearchSaga';
import getTodoListSaga from '../modules/todos/TodoSaga';


export const rootSaga = function* root() {
    yield all([
        fork(loginSaga),
        fork(changeSearchQSaga),
        fork(getGiphiesSaga),
        fork(getTodoListSaga),
        fork(registerSaga),
        fork(getProfileSaga),

    ]);
};
