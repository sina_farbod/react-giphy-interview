import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router';


import registerReducer from '../modules/auth/register/registerReducer'
import loginReducer from '../modules/auth/login/loginReducer'
import toastReducer from '../modules/toast/toastReducer'
import searchReducer from '../modules/search/searchReducer'
import todosReducer from '../modules/todos/todosReducer'
import profileReducer from '../modules/profile/profileReducer'




const rootReducer = (history) => combineReducers({
	router: connectRouter(history),

	registerReducer,
	loginReducer,
	toastReducer,
	searchReducer,
	todosReducer,
	profileReducer,
	
})


export default rootReducer