import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import createRootReducer from './reducers';
import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { rootSaga } from './sagas';
const sagaMiddleware = createSagaMiddleware();
export const History = createBrowserHistory()

const composeEnhancer = (process.env['NODE_ENV'] !== 'production' && window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__']) || compose;
const configureStore = createStore(
  createRootReducer(History),
  {},
  composeEnhancer(applyMiddleware(routerMiddleware(History), sagaMiddleware))
);

sagaMiddleware.run(rootSaga);

export default configureStore