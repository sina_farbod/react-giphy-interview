import { ConnectedRouter } from 'connected-react-router';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.scss';
import LoginScreen from './modules/auth/login/LoginScreen';
import RegisterScreen from './modules/auth/register/RegisterScreen';
import ProfileScreen from './modules/profile/ProfileScreen';
import SearchScreen from './modules/search/SearchScreen';
import MyToast from './modules/toast/MyToast';
import { hideToast } from './modules/toast/toastAction';
import TodoListScreen from './modules/todos/TodoListScreen';
import { PrivateRoute } from './navigations/PrivateRoute';
import { History } from './store/configureStore';




function App() {

  const dispatch = useDispatch()
  const { toast } = useSelector(state => state.toastReducer);

  const hideModal = () => dispatch(hideToast())
  console.log(`toast`, toast)

  return (
    <ConnectedRouter history={History}>
      {/* <Router> */}

        {
          toast !== '' &&
          <MyToast
            msg={toast}
            headerBg={`${toast && toast !== 401 && toast.includes("SUCCESS") ? "bg-success" : "bg-danger"}`}
            showModal={!!toast}
            close={hideModal}
          />
        }


        <Switch>
          <Route exact path="/" component={SearchScreen} />

          <Route path="/register" component={RegisterScreen} />
          <Route path="/login" component={LoginScreen} />
          <PrivateRoute path="/todo" component={TodoListScreen} />
          <PrivateRoute path="/profile" component={ProfileScreen} />

          {/* <Route component={notFound} /> */}

        </Switch>


      {/* </Router> */}
    </ConnectedRouter>
  );
}

export default App;